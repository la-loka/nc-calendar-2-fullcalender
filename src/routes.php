<?php
// Routes

date_default_timezone_set('Europe/Madrid');

use Sabre\VObject;
use Sabre\VObject\Recur;

function buildFullcalendarArray($vevent, $calendar_params){
    $e = array();

    $e['id']=(string)$vevent->{'UID'};
    $e['title']=(string)$vevent->{'SUMMARY'};
    $e['color']=$calendar_params['color'];
    $e['description'] = null;
    if (isset($vevent->DESCRIPTION)){
        $e['description']=(string)$vevent->{'DESCRIPTION'};
    }
    $e['location'] = null;
    if (isset($vevent->DESCRIPTION)){
        $e['location']=(string)$vevent->{'LOCATION'};
    }
    $e['start'] = $vevent->DTSTART->getDateTime()->format(\DateTime::W3C);
    $e['end'] = $vevent->DTEND->getDateTime()->format(\DateTime::W3C);

    $e['allDay']=false;  
    if (!$vevent->DTSTART->hasTime()){
        $e['allDay']=true;
    }
    return $e;
}

///feed?start=2013-12-01&end=2014-01-12&_=138605475138
///feed?callback=?
$app->get('/feed', function ($request, $response, $args) {
    
    $callback = $request->getParam('callback');
    if (!$callback){
        return null;
    }
    $calendarStart = strtotime($request->getParam('start'));
    $calendarEnd = strtotime($request->getParam('end'));
    if (!($calendarStart and $calendarEnd)){
        // missing/bad params
        return null;
    }
    if ($calendarStart >= $calendarEnd){
        // bad dates
        return null;
    }
    $result = array();
    $calendarStartDate = DateTime::createFromFormat('U', $calendarStart);
    $calendarEndDate = DateTime::createFromFormat('U', $calendarEnd);
    
    $db_params = $this->get('database');
    $db_conn = new PDO('mysql:host='.$db_params['host'].';dbname='.$db_params['name'],                    
                        $db_params['user'], $db_params['pass']);

    $sql =  'SELECT calendardata FROM oc_calendarobjects '.
            'WHERE '.
                'calendarid = :id AND '.
                'classification = :classification AND '.
                'firstoccurence <= :end AND '.
                'lastoccurence >= :start';
                
    $record = $db_conn->prepare($sql);
    
    foreach ($this->get('calendar')['params'] as $calendar_params){
        $row = $record->execute(array(
                            'id'=>$calendar_params['calendar_id'],
                            'classification'=>$calendar_params['classification'],
                            'start'=>$calendarStart,
                            'end'=>$calendarEnd
                        ));
        $rs = $record->fetchAll(PDO::FETCH_ASSOC);
           
        foreach ($rs as $vcalendar){
            $vcalendar = VObject\Reader::read($vcalendar['calendardata']);
            $vevent = $vcalendar->VEVENT;

            if (isset($vevent->RRULE)){
                $vstart = $vevent->DTSTART->getDateTime();
                $vend = $vevent->DTEND->getDateTime();
                $diff = $vstart->diff($vend);
                $rruleIterator = new VObject\Recur\RRuleIterator((string)$vevent->{'RRULE'}, $vstart);
                
                while ($rruleIterator->valid()){
                    $recurStart = $rruleIterator->current();
                    if ($recurStart > $calendarEndDate){
                        break;
                    }
                    $recurEnd = $recurStart->add($diff);
                    if ($recurEnd < $calendarStartDate){
                        $rruleIterator->next();
                        continue;
                    }
                    $vevent->DTSTART->setDateTime($recurStart);
                    $vevent->DTEND->setDateTime($recurEnd);
                    array_push($result, buildFullcalendarArray($vevent, $calendar_params));
                    $rruleIterator->next();
                }
            }else{
                array_push($result, buildFullcalendarArray($vevent, $calendar_params));
            }
        }
    }
    return "$callback(" . json_encode($result) . ");";
});

$app->get('/', function ($request, $response, $args) {
    // Render index view
    return $this->renderer->render($response, 'index.phtml', $args);
});
