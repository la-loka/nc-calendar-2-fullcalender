<?php
return [
    'settings' => [
        'displayErrorDetails' => true, // set to false in production
        'addContentLengthHeader' => false, // Allow the web server to send the content-length header

        // Renderer settings
        'renderer' => [
            'template_path' => __DIR__ . '/../templates/',
        ],
    ],
    'database'=>[
        'name'=>'nextcloud',
        'host'=>'localhost',
        'user'=>'root',
        'pass'=>'password',
    ],
    'calendar'=>[
        'params'=>array(
                    array('calendar_id'=>1, 'color'=>'#B0171F', 'classification'=>0),
                    array('calendar_id'=>2, 'color'=>'#00C957', 'classification'=>0),
                ),
    ],
];
