
This needs to be installed a server that can access the nextcloud database


* create subdomain my-calendar.my-domain.net
* download tarball http://git.entitatstransparents.org/?p=nextcloud-fullcalendar;a=snapshot;h=HEAD;sf=tgz
* unpack at /var/www/my-calendar
* create a apache/nginx config

```
root        /var/www/my-calendar/public/;
index       index.php;
```

* create a database user that has read access to the table oc_calendarobjects
* Find the id(s) of the calendar(s) in the table you want to share

* edit /var/www/my-calendar/src/settings.php

Goto http://my-calendar.my-domain.net

You can embed this into a wordpress page like this


```
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://my-calendar.my-domain.net/lib/moment.min.js"></script>
<script src="https://my-calendar.my-domain.net/lib/fullcalendar/ca.js"></script>
<script src="https://my-calendar.my-domain.net/lib/fullcalendar/fullcalendar.min.js"></script>
<script src="/wp-content/agenda/script.js"></script>
<div id="calendar"></div>
<div id="modal">
<div id="modal-content"></div>
<div id="modal-close">[tancar]</div>
</div>
```

Create a directory in wp-content called agenda

add a files calles script.js

```
$(document).ready(function() {
      var dayEvents = {};
      $('#calendar').fullCalendar({
            // put your options and callbacks here
            events: 'https://my-calendar-my-domain.net/feed?callback=?',
            firstDay: 1,
            timeFormat: 'H:mm',
            locale: 'ca',
            //defaultView: 'listMonth',
            defaultView: $(window).width() < 765 ? 'listMonth':'month',
            height: 1000,
            //contentHeight: 1250,
            //aspectRatio: 1,
            eventClick: function(data, jsEvent, view) {
                var day = moment(data.start).format("DD/MM/YY");
                var content = $(this).find('.desc').html();
                $('#modal-content').html(content);
                //$('#modal-content').html(dayEvents[day].html());
                //console.log(dayEvents[day]);
                $("#modal").addClass("active");
            },
            eventRender: function(event, element) {

                $content = $('<div class="desc" style="display:none"></div>');
                $content.append('<strong>'+event.title+'</strong><br />');
                if (event.location != null) {
                        $content.append(event.location+' : ');
                }
                $content.append(moment(event.start).format('DD/MM/YY HH:mm')+'h - '+moment(event.end).format('HH:mm')+'h');
                $content.append('<hr style=" margin:0; padding:0"/>');
                $content.append(event.description);
                element.append($content);
                return element;
            },
    });
        $("#modal-close").click(function () {
                $("#modal").removeClass("active");
        });
});
```


